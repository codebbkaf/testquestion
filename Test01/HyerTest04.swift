//
//  HyerTest04.swift
//  Test01
//
//  Created by HankTseng on 2019/4/2.
//  Copyright © 2019 HyerDesign. All rights reserved.
//

import Foundation
/*
dispatch_queue_t queue = dispatch_queue_create("my.label", DISPATCH_QUEUE_SERIAL);
dispatch_async(queue, ^{
    dispatch_sync(queue, ^{
        // outer block is waiting for this inner block to complete,
        // inner block won't start before outer block finishes
        // => deadlock
        });
    
    // this will never be reached
    });
*/
func fetchData() {
    let aSerialQueue = DispatchQueue(label: "my.custom.get.data")
    print("1")
    aSerialQueue.async {
        print("2")
//        DispatchQueue.main.async {
//             print("3")
//        }
        aSerialQueue.async {
            print("3")
            // outer block is waiting for this inner block to complete,
            // inner block won't start before outer block finishes
            // => deadlock
            // 外面的block正在等裡面的block執行完才會往下執行[37]，但是裡面的block要等到外面的block執行完才開始 = deadlock。
            // 如上改成 aSerialQueue.async 破解dead lock
        }
        print("4")
        // this will never be reached
    }
    print("5")
    
    //no dead lock 15234 or 15243
    //deadlock 152
}
