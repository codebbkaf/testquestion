//
//  HyerTest.swift
//  Test01
//
//  Created by HankTseng on 2019/3/28.
//  Copyright © 2019 HyerDesign. All rights reserved.
//

import Foundation

protocol SendDataDelegate: class {  //obey only class for weak
    func processData()
}

class SendingVC {
    weak var delegate: SendDataDelegate?  // weak var 避免retain cycle
    deinit {
        print("SendingVC well gone, bruh")
    }
}

class ReceivingVC: SendDataDelegate {
    
    var sendingVC: SendingVC!
    
    init() {
        print("test1 init")
        sendingVC = SendingVC()
        sendingVC.delegate = self
    }
    
    func processData() {
        print("processData")
    }
    
    deinit {
        print("ReceivingVC well gone, bruh")
    }
}
