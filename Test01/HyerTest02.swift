//
//  HyerTest02.swift
//  Test01
//
//  Created by HankTseng on 2019/3/28.
//  Copyright © 2019 HyerDesign. All rights reserved.
//

import Foundation

class BobClass {
    var bobClosure: (() -> ())?
    var name = "Bob"
    init() {
        print("init")
        self.bobClosure = { [weak self] in //[weak self] in
            let name = self?.name   //self?.name closure中避免retain cycle
            
            print("\(name) the Developer")
        }
    }
    deinit {
        print("I’m gone... ☠️")
    }
}
