//
//  HyerTest03.swift
//  Test01
//
//  Created by HankTseng on 2019/3/29.
//  Copyright © 2019 HyerDesign. All rights reserved.
//

import Foundation

protocol Animal {
    associatedtype Food
    func eat(food: Food)
}

protocol BookStore {
    func sell(book: String)
}

class Dog: Animal {
    typealias Food = String
    func eat(food: Food) {
        print("dog eat \(food)")
    }
}

class Cat: Animal {
    enum CatFavoriteFood: String {
        case fish = "fish"
        case milk = "milk"
    }
    typealias Food = CatFavoriteFood
    func eat(food: Food) {
        print("cat eat \(food.rawValue) fishes")
    }
    
    //associatedtype在runtime才決定型別，但是swift是型別安全語言必須在宣告的時候就給他型別
    //Protocol 'Animal' can only be used as a generic constraint because it has Self or associated type requirements
    var animalProtocol: Any?
    
    var bookStoreProtocol: BookStore?
}


